import axios from 'axios';
import {endpoint} from "../const/api";

const API = axios.create({
  baseURL: endpoint,
});

API.interceptors.request.use(
  async config => {
    const id_token = await localStorage.getItem('id_token');
    config.headers = {
      'Authorization': `Bearer ${id_token}`,
      'Accept': 'application/json',
    }
    return config;
  },
  error => {
    Promise.reject(error)
  });

export default API;
