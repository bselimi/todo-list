import {Button, Input} from "reactstrap";
import React, {useState} from "react";

function AddTodo({addTodo, textChanged}) {
  const [text, setText] = useState('');

  const addTodoTextChanged = e => {
    const text = e.target.value;
    setText(text);
    if (textChanged) textChanged(text);
  };

  return (
    <div className="add-todo">
      <div className="row">
        <div className="col-6">
          <Input type="text" placeholder="Enter a TODO" value={text} onChange={addTodoTextChanged} />
        </div>
        <div className="col-3">
          <Button color="primary" disabled={text.trim().length <= 4} onClick={() => addTodo(text)}>Add todo</Button>
        </div>
      </div>
    </div>
  );
}

export default AddTodo;
