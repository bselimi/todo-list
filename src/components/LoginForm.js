import {useState} from "react";
import {endpoint} from "../const/api";
import {ThemeContext} from "../context/theme-context";
import {useContext} from "react";
import {useUser} from "../context/user-context";

function LoginForm() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  const {loggedIn, user, login} = useUser();

  const {buttonStyle} = useContext(ThemeContext);

  const isFormValid = email.length >= 6 && password.length >= 8;

  const submitForm = () => {
    login({email, password});
    // .finally(loadTodos);
  };

  return (
    <>

      <div className="row justify-content-md-center">
        <div className="col-4">
          <h2>Login Form</h2>
          {error && <div className="alert alert-danger" role="alert">Wrong username or password!</div>}
          <div className="form-group">
          <label htmlFor="exampleInputEmail1">Email address</label>
          <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                 placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} />
            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone
              else.</small>
        </div>
        <div className="form-group">
          <label htmlFor="exampleInputPassword1">Password</label>
          <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password"
                 value={password} onChange={e => setPassword(e.target.value)}/>
        </div>
        <div className="form-check">
          <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
          <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
        </div>
        <button disabled={!isFormValid} type="submit" className={"btn btn-" + buttonStyle} onClick={submitForm}>Submit</button>
          <p>
            Email: {email}<br/>
            Password: {password}
          </p>
      </div>
      </div>
    </>
  );
}

export default LoginForm;
