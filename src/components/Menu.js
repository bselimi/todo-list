import {Link} from "react-router-dom";
import React from "react";
import {useUser} from "../context/user-context";

function Menu() {
  const {loggedIn, logout} = useUser();

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <a className="navbar-brand" href="#">Navbar</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item active">
            <Link className="nav-link" to="/">Home</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/todo-list">Todo List</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/about-us">About Us</Link>
          </li>
          {loggedIn || <li className="nav-item">
            <Link className="nav-link" to="/login">Login</Link>
          </li>}
          {loggedIn && <li className="nav-item">
            <span style={{cursor: 'pointer'}} className="nav-link" onClick={() => logout()}>Logout</span>
          </li>}
        </ul>
      </div>
    </nav>
  );
}

export default Menu;
