import {Link, useParams} from "react-router-dom";
import Todo from "./Todo";
import React, {useMemo} from "react";

function SingleTodo({todos, deleteTodo, toggleDone, url}) {
  let { todoId } = useParams();

  const todo = useMemo(() => {
    console.log('Finding todo:', todoId, todos);
    if (!todos || !todoId) return null;
    return todos.find(it => it.id === parseFloat(todoId));
  }, [todos, todoId]);

  return (
    <>
      <Link to={url}>
        Back to list
      </Link>
      <Todo key={todo.id} todo={todo} onDeleteTodo={deleteTodo} onToggleDone={toggleDone}/>
    </>
  );
}

export default SingleTodo;
