import React, {useMemo, useState, useEffect} from 'react';
import './TodoList.css';
import Todo from "./Todo";
import {Badge, ListGroup} from "reactstrap";
import {endpoint} from "../const/api";
import AddTodo from "./AddTodo";
import {
  Switch,
  Route,
  useRouteMatch
} from "react-router-dom";
import SingleTodo from "./SingleTodo";
import {useUser} from "../context/user-context";
import API from "../api/api";

function TodoList() {
  let match = useRouteMatch();

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [todos, setTodos] = useState([]);
  const {token} = useUser();

  const loadTodos = () => {
    setLoading(true);
    API.get('/todos')
      .then(response => setTodos(response.data))
      .catch(() => setError('Error loading todos'))
      .finally(() => setLoading(false));

  }

  useEffect(loadTodos, [token]);

  const notDoneCount = useMemo(
    () => todos.filter(todo => !todo.done).length,
    [todos]
  );

  const toggleDone = todo => {
    todo.done = !todo.done;
    API.put(`/todos/${todo.id}`, todo)
      .then(response => setTodos([...todos]));
  };

  const addTodo = (text) => {
    const todo = {
      text: text.trim(),
      done: false,
    };
    API.post(`/todos`, todo)
      .then(response => setTodos([response.data, ...todos]));
  };

  const deleteTodo = todo => {
    API.delete(`/todos/${todo.id}`)
      .then(() => {
        setTodos(todos.filter(t => t !== todo));
        setError(null);
      });
  };

  const textChanged = () => {
    setError(null);
  }

  if (loading) {
    return <div className="loader">Loading...</div>;
  }

  return (
    <>
      <Switch>
        <Route path={`${match.path}/:todoId`}>
          <SingleTodo todos={todos} url={match.url} deleteTodo={deleteTodo} toggleDone={toggleDone} />
        </Route>

        <Route path={match.path}>
          <AddTodo addTodo={addTodo} textChanged={textChanged}/>

          {error && <div className="error">{error}</div> }

          <div>Remaining:
            <Badge pill color="primary">{notDoneCount}</Badge>
            out of
            <Badge pill color="secondary">{todos.length}</Badge>
          </div>

          <ListGroup>{
            todos.map(todo => <Todo key={todo.id} url={match.url} todo={todo} onDeleteTodo={deleteTodo} onToggleDone={toggleDone} />)
          }
          </ListGroup>
        </Route>
      </Switch>
    </>
  );
}

export default TodoList;
