import React from "react";
import {Button, Input, ListGroupItem} from "reactstrap";
import {Link, useRouteMatch} from "react-router-dom";

function Todo({todo, onDeleteTodo, onToggleDone}) {
  let match = useRouteMatch();
  return (
    <ListGroupItem>
      <div className="row">
        <div className="col-1">
          <Input className="ml-3" type="checkbox" checked={todo.done} onChange={() => onToggleDone(todo)} />
        </div>
        <div className="col-8">
          {todo.id}:
          <Link to={`${match.url}/${todo.id}`}>
          {
            todo.done ? <del>{todo.text}</del> : todo.text
          }
          </Link>
        </div>
        <div className="col-2">
          {todo.created_at && (new Date(todo.created_at)).toLocaleString()}
        </div>
        <div className="col-1">
          <Button close onClick={() => onDeleteTodo(todo)} />
        </div>
      </div>
    </ListGroupItem>
  )
}

export default Todo;
