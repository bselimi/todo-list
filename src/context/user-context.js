import {createContext} from "react";
import {useContext, useState, useEffect} from "react";
import {endpoint} from "../const/api";
import API from "../api/api";


const LOGGED_OUT_USER = {
  loggedIn: false,
  token: null
};

const token = localStorage.getItem('id_token') || null;

const defaultUser = {
  loggedIn: Boolean(token),
  token,
};

const user = defaultUser;

const UserContext = createContext(defaultUser);

export const useUser = () => useContext(UserContext);

export function ProvideUserContext({children}) {
  const user = useProvideUser();
  return <UserContext.Provider value={user}>{children}</UserContext.Provider>;
}

function useProvideUser() {
  const [user, setUser] = useState(defaultUser);

  useEffect(() => {
    // Response interceptor for API calls
    API.interceptors.response.use((response) => {
      return response
    }, async function (error) {
      const originalRequest = error.config;
      if (error.response.status === 401) {
        setUser(LOGGED_OUT_USER);
      }
      return Promise.reject(error);
    });
  }, []);

  const login = (data) => {
    API.post('/login', data)
      .then(response => {
        const { data } = response;
        if (data.access_token) {
          localStorage.setItem('id_token', data.access_token);
          setUser({
            loggedIn: true,
            token: data.access_token,
          });
          console.log('Succes:', data.access_token);
        } else if (data.error) {
          console.log('Error:', data.error);
          setUser(defaultUser);
        }
      });
  };

  const logout = () => {
    localStorage.removeItem('id_token');
    setUser(LOGGED_OUT_USER);
  }

  // const user = useContext(ThemeContext);
  return {
    ...user,
    login,
    logout
  };
}
