import {createContext} from "react";

export const themes = {
  light: {
    buttonStyle: "primary",
    foreground: "#000000",
    background: "#eeeeee"
  },
  dark: {
    buttonStyle: "secondary",
    foreground: "#ffffff",
    background: "#222222"
  }
};

export const ThemeContext = createContext(themes.light);
