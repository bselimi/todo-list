import React from 'react';
import './App.css';
import Home from "./components/Home";
import TodoList from "./components/TodoList";
import AboutUs from "./components/AboutUs";
import LoginForm from "./components/LoginForm";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Menu from "./components/Menu";
import {useUser} from "./context/user-context";

function App() {
  const {loggedIn} = useUser();

  return (
    <Router>
      <div className="App">
        {loggedIn && <Menu/>}

        <div className="container">
          <main>
            {loggedIn && <Switch>
              <Route path="/about-us">
                <AboutUs/>
              </Route>
              <Route path="/todo-list">
                <TodoList/>
              </Route>
              <Route path="/">
                <Home/>
              </Route>
            </Switch>
            }
            {!loggedIn && <LoginForm/>}
          </main>
        </div>
      </div>
    </Router>
  );
}

export default App;
